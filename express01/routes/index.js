var express = require('express');
var router = express.Router();

const Test01 = require('../models/cls/Test01')
const multer = require('multer')
const upload = multer({dest: './uploads/' }).single('file')
const fs = require('fs')
const s3 = require('../models/utils/s3')

/* GET home page. */
router.get('/', function(req, res, next) {
  const test01 = new Test01()
  
  res.render('index', { title: test01.getTest01()});
});

router.get('/page01', (req, res, next) => {
  res.render('page01/index')
})

router.get('/page01/list', (req, res, next) => {
  res.render('page01/list')
})

router.get('/page01/test', (req, res, next) => {
  res.render('page01/test')
})

router.get('/page01/file', (req, res, next) => {
  res.render('page01/file')
})

router.post('/page01/fileentry', (req, res, next) => {
  upload(req, res, (err) => {
    if(err) console.log(err);
    else{
      console.log(req.file);
      fs.readFile(req.file.destination + req.file.filename, (err2, data2) => {
        console.log(data2)
        let event = {}
        event.Key = req.file.originalname;
        event.Body = data2
        s3.handler(event)
      })
    }
  })

  res.render('page01/fileentry')
})

module.exports = router;
