var gulp = require('gulp');
var babel = require('gulp-babel');

gulp.task('babel', function(){
  console.log('start');
  gulp.src(['src/*.js', 'src/**/*.js'])
    .pipe(babel())
    .pipe(gulp.dest('lib'));
    console.log('end');
    
});

gulp.task('watch', function(){
  gulp.watch('./src/*.js', ['babel']);
});

gulp.task('default',['babel', 'watch']);