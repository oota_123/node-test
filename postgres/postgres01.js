const { Client } = require('pg')
const client = new Client()

exports.hensu = async() => {
    console.log('start')
    
    const test = 'sample'
    console.log(`test${test}`);
    
}

// update
exports.test01 = async() => {
    await client.connect()

    const no = 1
    const text = 'update desu'
    const sql = `update t01 set text = '${text}' where no = ${no}`
    await client.query(sql)

    await client.end()
}

// select
exports.select = async() => {
    await client.connect()

    const sql = "select * from t01"
    const res = await client.query(sql)

    res.rows.forEach(row => {
        console.log(row);
    })

    await client.end()
}

// connect
exports.connect = async () => {
    console.log('postgres01');
 
    await client.connect()
    
    const res = await client.query('SELECT NOW() now')
    console.log(res.rows[0].now);
    
    await client.end()
}