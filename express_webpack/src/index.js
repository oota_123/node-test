import {sayHello} from "./Hello.js"

const fnc01 = async () => {
    await fnc02()
    alert('fnc01 end')
}

const fnc02 = () => {
    return new Promise((resolve) => {
        setTimeout(() => {
            alert('fnc02')
            resolve()
        }, 1000)
    })
}

fnc01()

