const AWS = require('aws-sdk');

// proxy環境の場合
const proxy = require('proxy-agent');
AWS.config.update({region: 'ap-northeast-1',
    httpOptions: {agent: proxy('http://mtc-px14:8081')}});

exports.handler = async (event) => {
    const params = {
        Bucket: "otlambda",
        Key: "labmda2.txt",
        Body: "sample"
    }
    
    await putS3(params);
    
    // TODO implement
    const response = {
        statusCode: 200,
        body: JSON.stringify('Hello from Lambda!'),
    };
    // return response;
};

const putS3 = (params) => {
    const s3 = new AWS.S3();    
    
    return new Promise((resolve) => {
        s3.putObject(params, (err, data) => {
        if(err) console.log(err, err.stack);
        else    {console.log('success'); resolve();}
        })
    })
}