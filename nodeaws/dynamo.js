const aws = require('aws-sdk')

// proxy環境の場合
const proxy = require('proxy-agent');
aws.config.update({region: 'ap-northeast-1',
    httpOptions: {agent: proxy('http://mtc-px14:8081')}});

const docClient = new aws.DynamoDB.DocumentClient({region: 'ap-northeast-1'});

exports.handler = async(event) => {
    const params = {
        TableName: 'Test01',
        Item: {
            Primary01: 'Ota3',
            val1: 'atai',
            list: {
                list1: 'atai'
            }
        }
    };
    
    docClient.put(params, (err, data) => {
        if(err) console.log(err)
        else console.log('success')
    })
}