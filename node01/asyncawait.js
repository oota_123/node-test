const fnc01 = () => {
    return 100
}

const fnc02 = async () => {
    console.log("start")

    await fnc02_2();
    await fnc02_2();

    console.log("end")
    
}

const fnc02_2 = () => {
    return new Promise((resolve,reject) => {
        setTimeout(() => {
            console.log('setTimeout')
            resolve()
        }, 1000)
    })
}

exports.test01 = () => {
    fnc02()
}