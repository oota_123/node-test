const fs = require('fs')

// read & write
exports.test01 = () => {
    fs.readFile("tmpfiles/test.xlsx", (err, data) => {
        console.log(data)

        fs.writeFile("tmpfiles/test_copy.xlsx", data, (err) => {
            if(err) console.log(err);
            
        })
    })    
}

// writeFile
exports.write = () => {
    fs.writeFile("tmpfiles/test.txt","テスト",(data) => {
    });
}

